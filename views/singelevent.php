<div class="sample-container">
	<div class="sample-data">
		<table class="showtable2">
			<tbody>
				<tr>
					<td class="date2">
						<center>
							{{ date }}
						</center>
					</td>
					<td class="venue2">
						{{ name }}
					</td>
				</tr>
				<tr>
					<td></td>
					<td class="venue2">
						<p>
							<b>What:</b> {{what}}
						</p>
						<p>
							<b>Where:</b> {{ where }}
						</p>
						<p>
							<b>When:</b> {{date}}
						</p>
						<p>
							<b>Damage:</b> {{damage}}
						</p>
					</td>
				</tr>
			</tbody>
		</table>
	</div>{{ pagination:links }}
</div>