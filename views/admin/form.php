<section class="title">
	<!-- We'll use $this->method to switch between sample.create & sample.edit -->
	<h4><?php echo lang('sample:'.$this->method); ?></h4>
</section>

<section class="item">

	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		
		<div class="form_inputs">
	
		<ul>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="name"><?php echo lang('gigdate:name'); ?><span>*</span></label>
				<div class="input"><?php echo form_input('name', set_value('name', $name), 'class="width-15"'); ?></div>
			</li>

			<li class="date-meta">
				<label><?php echo lang('gigdate:date'); ?><span>*</span></label>
				
				<div class="input datetime_input">
				<?php echo form_input('created_on', date('Y-m-d', $rightdate), 'maxlength="10" id="datepicker" class="text width-20"'); ?> &nbsp;
				<?php echo form_dropdown('created_on_hour', $hours, date('H', $rightdate)) ?> : 
				<?php echo form_dropdown('created_on_minute', $minutes, date('i', $rightdate)); ?>
				
				</div>
			</li>
			<li>
				<label for="dontshowtime"><?php echo lang('gigdate:dontshowtime');?></label>
				<div class="input"><?php echo form_checkbox('dontshowtime', 1, $dontshowtime, 'class="width-15"'); ?></div>
			</li>
			
			<li>
				<label for="published"><?php echo lang('gigdate:published');?></label>
				<div class="input"><?php echo form_checkbox('published', 1, $published, 'class="width-15"'); ?></div>
			</li>
			

			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="description"><?php echo lang('gigdate:description'); ?></label>
				<?php echo form_textarea(array('id' => 'description', 'name' => 'description', 'value' => $description, 'rows' => 30, 'class' => 'wysiwyg-simple')); ?>
			</li>

			<li class="<?php echo alternator('', 'uneven'); ?>">
				<label for="city"><?php echo lang('gigdate:city'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('city', set_value('city', $city), 'class="width-15"'); ?></div>
			</li>
			
			<li class="<?php echo alternator('', 'uneven'); ?>">
				<label for="province"><?php echo lang('gigdate:province'); ?></label>
				<div class="input"><?php echo form_input('province', set_value('province', $province), 'class="width-15"'); ?></div>
			</li>
			
			<li class="<?php echo alternator('', 'uneven'); ?>">
				<label for="avenue"><?php echo lang('gigdate:avenue'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('avenue', set_value('avenue', $avenue), 'class="width-15"'); ?></div>
			</li>
			
			<li class="<?php echo alternator('', 'uneven'); ?>">
				<label for="avenuelink"><?php echo lang('gigdate:avenuelink'); ?></label>
				<div class="input"><?php echo form_input('avenuelink', set_value('avenuelink', $avenuelink), 'class="width-15"'); ?></div>
			</li>
			
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="price"><?php echo lang('gigdate:price'); ?></label>
				<div class="input"><?php echo form_input('price', set_value('price', $price), 'class="width-15"'); ?></div>
			</li>
			
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="age"><?php echo lang('gigdate:age'); ?></label>
				<div class="input"><?php echo form_input('age', set_value('age', $age), 'class="width-15"'); ?></div>
			</li>
			
			<li class="<?php echo alternator('', 'even'); ?>">
			<label for="age"><?php echo lang('gigdate:curlabel'); ?></label>
			<div class="input"><?php echo form_dropdown('currency', $cur_values, $currency) ?></div>
			</li>

			<li class="<?php echo alternator('', 'uneven'); ?>">
				<label for="support"><?php echo lang('gigdate:support'); ?></label>
				<div class="input"><?php echo form_input('support', set_value('support', $support), 'class="width-15"'); ?></div>
			</li>
		</ul>
		
		</div>
		
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
		</div>
		
	<?php echo form_close(); ?>

</section>
