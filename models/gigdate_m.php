<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
	* this file is part of a gigdate module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a gigdate module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Gigdate Module
 */
 
class GigDate_m extends MY_Model {
	
	/**
	 * Constructor method
	 *
	 * @access public
	 * @return void
	 */
	public function __construct()
	{		
		parent::__construct();
		
		$this->_table = 'gig_date';
	}
	
	/**
	 * create a new item
	 *
	 * @access public
	 * @param gigdate object
	 * @return bool
	 */

	// public function create($input)
	// {
	// 	$to_insert = array(
	// 		'name' => $input['name'],
	// 		'date' => $input['date'],	
	// 	);

	// 	return $this->db->insert($this->_table, $to_insert);
	// }

	public function edit($id, $input)
	{
		$this->db->trans_start();


		// validate the data and update
		$result = $this->update($id, array(
			'name'				=> $input['name'],
			'date'				=> $input['date'],
			'published'			=> ! empty($input['published']),
			'dontshowtime'		=> ! empty($input['dontshowtime']),
			'description'		=> $input['description'],
			'city'				=> $input['city'],
			'province'			=> $input['province'],
			'avenue'			=> $input['avenue'],
			'avenuelink'		=> $input['avenuelink'],
			'price'				=> $input['price'],
			'age'				=> $input['age'],
			'currency'			=> $input['currency'],
			'support'			=> $input['support'],

		));



		// did it pass validation?
		if ( ! $result) return false;

		$input['id'] = $id;

		$this->db->trans_complete();

		return (bool) $this->db->trans_status();
	}

	public function create($input)
	{
		$this->db->trans_start();


		// validate the data and update
		$result = $this->insert(array(
			'name'				=> $input['name'],
			'date'				=> $input['date'],
			'published'			=> ! empty($input['published']),
			'dontshowtime'		=> ! empty($input['dontshowtime']),
			'description'		=> $input['description'],
			'city'				=> $input['city'],
			'province'			=> $input['province'],
			'avenue'			=> $input['avenue'],
			'avenuelink'		=> $input['avenuelink'],
			'price'				=> $input['price'],
			'age'				=> $input['age'],
			'currency'			=> $input['currency'],
			'support'			=> $input['support'],

		));



		// did it pass validation?
		if ( ! $result) return false;

		// $input['id'] = $id;

		$this->db->trans_complete();

		return (bool) $this->db->trans_status();
	}
	
	/**
	* get an gigdate item by key,val
	*
	* @access public
	* @param key
	* @param value
	* @param format
	* @return mixed
	*/
	
	public function get_by($key,$val,$format = '')
	{
		$result = parent::get_by($key,$val);
		if (isset($format))
		if ($format == 'clean')
		{
				$date = new DateTime();
				$date = $date->createFromFormat('Y-m-d H:i:s', $result->date);
				$result->date = $date->format( 'd M' );
		}
		return $result;
	}
	
	/**
	* get all gigdate gigdate items
	*
	* @access public
	* @param format
	* @return mixed
	*/
	
	public function get_all_widget($format = 'raw', $past = false, $ascdesc = 'asc', $shownrof = 10)
	{
		$date = new DateTime();
		$date->sub(new DateInterval('P' . $this->settings->show_after . 'D'));
		$date = date_format($date, 'Y-m-d H:i:s');
		
		if ($past)
		{
			$results = $this->db
			->order_by('date' , $ascdesc)
			->where('date <', $date)
			->where('published', '1')
			->get($this->_table, $shownrof,0)
			->result();
		}
		else
		{
			$results = $this->db
			->order_by('date' , $ascdesc)
			->where('date >', $date)
			->where('published', '1')
			->get($this->_table, $shownrof,0)
			->result();	
		}

		if (isset($format))
		if ($format == 'clean')
		{
			$date = new DateTime();
			$_events = array();
			$tempar = array();
			foreach ($results as $event)
			{
				$date = $date->createFromFormat('Y-m-d H:i:s', $event->date);
				$tempar = $event;
				$tempar->day = $date->format( 'd' );
				$tempar->month = $date->format( 'M' );
				$tempar->dayofweek = $date->format( 'D' );
				$tempar->timeofday = $date->format( 'h:i a' );
				$tempar->year = $date->format( 'Y' );
				$_events[] = $tempar;
			}
			$results =&  $_events;
			}	
		return $results;
	}	
	
	public function get_all($format = 'raw')
	{
		$date = new DateTime();
		$date->sub(new DateInterval('P' . $this->settings->show_after . 'D'));
		$date = date_format($date, 'Y-m-d H:i:s');
		
		$results = $this->db
		->order_by('date' , $this->settings->gig_ascdesc)
		->where('date >', $date)
		->where('published', '1')
		->get($this->_table)
		->result();

		if (isset($format))
			if ($format == 'clean')
			{
				$date = new DateTime();
				$_events = array();
				$tempar = array();
				foreach ($results as $event)
				{
					$date = $date->createFromFormat('Y-m-d H:i:s', $event->date);
					$tempar = $event;
					$tempar->day = $date->format( 'd' );
					$tempar->month = $date->format( 'M' );
					$tempar->dayofweek = $date->format( 'D' );
					$tempar->timeofday = $date->format( 'h:i a' );
					$tempar->year = $date->format( 'Y' );
					$_events[] = $tempar;
				}
				$results =&  $_events;
			}	
		return $results;
	}	
	
	/**
	* get all gigdate items for admin panel
	*
	* @access public
	* @param format
	* @return mixed
	*/
	
	public function get_all_admin($format = 'raw')
	{
		$date = new DateTime();
		$date->sub(new DateInterval('P' . $this->settings->show_after . 'D'));
		$date = date_format($date, 'Y-m-d H:i:s');
		
		$results = $this->db
		->get($this->_table)
		->result();

		if (isset($format))
			if ($format == 'clean')
			{
				$date = new DateTime();
				$_events = array();
				$tempar = array();
				foreach ($results as $event)
				{
					$date = $date->createFromFormat('Y-m-d H:i:s', $event->date);
					$tempar = $event;
					$tempar->day = $date->format( 'd' );
					$tempar->month = $date->format( 'M' );
					$tempar->dayofweek = $date->format( 'D' );
					$tempar->timeofday = $date->format( 'h:i a' );
					$tempar->year = $date->format( 'Y' );
					$_events[] = $tempar;
				}
				$results =&  $_events;
			}	
		return $results;
	}	
}
