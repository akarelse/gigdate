<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
	* this file is part of a gigdate module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a gigdate module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Gigdate Module
 */
 
class Widget_GigDates extends Widgets
{
	public $title		= array(
		'en' => 'Gigdates',
	);
	public $description	= array(
		'en' => 'Show a list of gig dates'
	);
	public $author		= 'Aat Karelse';
	public $website		= 'http://vuurrosmedia.nl';
	public $version		= '1.0';
	
  public $fields = array(
		array(
			'field' => 'nrof',
			'label' => 'Nr of items',
			'rules' => 'required'
		),
		array(
			'field' => 'gigextra',
			'label' => 'Gig extra',
			'rules' => 'required'
		),
		array(
			'field' => 'ascdesc',
			'label' => 'Acending / Descending',
			'rules' => 'required'
		)
	);
  
	/**
	* Run widget
	*
	* @access public
	* @return array
	*/
	public function run($options)
	{
		$this->lang->load('gigdate/gigdate');
		$events = $this->gigdate_m->get_all_widget('clean', false,$options['ascdesc'], $options['nrof']);

		if (count($events))
		{
			$items_exist = TRUE;
		}
		else
		{
			$items_exist = FALSE;
		}

		return array('events' => $events,'items_exist' => $items_exist,'options' => $options);
	}	
}
