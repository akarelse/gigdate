{{ if items_exist == false }}
<p>
	There are no items.
</p>{{ else }} {{events }}
<div class="tour_date_row">
	<div class="show_holder">
		<div class="date_holder">
			<h2>
				<span class="day_num">{{month}}</span> <span class="month">{{day}}</span> <span class="day_text">{{dayofweek}}</span>
			</h2>
		</div>
		<div class="show_info_holder">
			<div class="show_head">
				<span class="city-state">{{city}}{{ if province != '' }}, {{province}} {{ endif }}</span> <span class="venue_name">{{ if avenuelink != '' }} at <a href="{{avenuelink}}" alt="{{avenue}}" title="{{avenue}}">{{avenue}}</a> {{ else }} at {{avenue}} {{ endif }} {{ if support != '' }} w/ {{support}} {{ endif }}</span>
			</div>
		</div>{{ if settings:gig_extra == 1}}
		<div data-id="tourdate1" class="more_button">
			MORE INFO
		</div>
	</div>
	<div class="show_more_info">
		<table>
			<tbody>
				<tr>
					<td class="show_more_info_comments_td">
						<div id="tourdate12" class="show_more_info_comments">
							<span class="whitespan">{{description}}</span>
						</div>
					</td>
					<td class="show_more_info_details">
						<h3>
							Details
						</h3>
						<ul>
							<li>
								<span class="show_more_info_field">Venue</span> {{ if avenuelink != '' }} <span class="show_more_info_value"><a target="_blank" href="{{avenuelink}}" alt="{{avenue}} Link" title="{{avenue}}">{{avenue}}</a></span> {{else}} <span class="show_more_info_value">{{avenue}}</span> {{ endif }}
							</li>
							<li>
								<span class="show_more_info_field">City</span> <span class="show_more_info_value">{{city}}{{ if province != '' }}, {{province}} {{ endif }}</span>
							</li>
							<li>
								<span class="show_more_info_field">Date</span> <span class="show_more_info_value">{{dayofweek}} {{month}} {{day}}, {{year}}</span>
							</li>
							<li style="list-style: none">{{ if dontshowtime != '1' }}
							</li>
							<li>
								<span class="show_more_info_field">Doors</span> <span class="show_more_info_value">{{ timeofday }}</span>
							</li>
							<li style="list-style: none">{{ endif }}
							</li>
							<li>
								<span class="show_more_info_field">Ages</span> {{ if age != '' }} <span class="show_more_info_value">{{age}}</span> {{else}} <span class="show_more_info_value">All Ages</span> {{ endif }}
							</li>
							<li>
								<span class="show_more_info_field">Price</span> {{ if price != '0' and price != ''}} <span class="show_more_info_value">{{ if currency == 0 }} {{settings:currency}} {{elseif currency == 1}} $ {{elseif currency == 2}} € {{elseif currency == 3}} £ {{elseif currency == 4}} <?php echo lang('eventdate:currency');?> {{endif}} {{price}}</span> {{else}} <span class="show_more_info_value">Free</span> {{ endif }}
							</li>
						</ul>
						<div class="more_button-up">
							hide
						</div>
					</td><!-- /show detials -->
				</tr>
			</tbody>
		</table>
	</div>{{ endif }}
</div>{{ /events }} {{ pagination:links }} {{ endif }}