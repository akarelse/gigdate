<?php

/**
	* this file is part of a gigdate module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a gigdate module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Gigdate Module
 */

//messages
$lang['gigdate:success']		=	'It worked';
$lang['gigdate:error']			=	'It didn\'t work';
$lang['gigdate:no_items']		=	'No Items';

//page titles
$lang['gigdate:create']		=	'Create Item';

//labels
$lang['gigdate:name']			=	'Name';
$lang['gigdate:date']			=	'Date';
$lang['gigdate:description']	=	'Description';
$lang['gigdate:city']			=	'City';
$lang['gigdate:province']		=	'State';
$lang['gigdate:avenue']		=	'Avenue';
$lang['gigdate:avenuelink']	=	'Avenue-link';
$lang['gigdate:price']			=	'Price';
$lang['gigdate:support']		=	'Support';
$lang['gigdate:item_list']		=	'Item List';
$lang['gigdate:view']			=	'View';
$lang['gigdate:edit']			=	'Edit';
$lang['gigdate:delete']		=	'Delete';
$lang['gigdate:dontshowtime']	= 	'Dont show hour and minutes.';
$lang['gigdate:published']		= 	'Published ?';
$lang['gigdate:curlabel']		= 	'Currency';

//buttons
$lang['gigdate:custom_button']	=	'Custom Button';
$lang['gigdate:items']			=	'Items';
$lang['gigdate:age']			=	'Minimal age';

//currency
$lang['gigdate:currency']	=	'&pound;';
?>
