# Gig date for PyroCMS

## Legal

This module was originally written by [Aat Karelse](http://vuurrosmedia.nl)
It is licensed under the version 3 of the GNU General Public License. [GPL license](http://www.gnu.org/licenses/)
More information about this module can be found at [more information](http://modules.vuurrosmedia.nl)

---

## Overview

Gigdate is a backend, frontend module for PyroCMS and it supports version 2.1.x 
It is a module that adds band and other event dates to your site.

---

## Features

- add gig date and time.
- add a state, city, avenue and avenue link.
- add a price and a currency.
- add the support.
- gives you the options to show the extra info and to publish or not.

---

## Widgets

Widget for the dates to come
Widget for the dates that have been

---

## Settings

Show extra gig info or not.
Nr of events to show.
Gig list ascending or descending.
Keep showing gig x nr of days after event took place.

---

## Installation

Download the archive and upload via Control Panel or Download the module from bitbucket.
Install the module.

---

## Credits

- Jerel Unruh for making available the sample module.
- The Pyro Dev team for making PyroCMS

---

## ToDo
+ Better translations and more
+ Cleaning up the code
+ Documentation
+ Inclusion of js to fold unfold blocks
+ Setting for max dates in the passed date widget
+ test if it works for more recent versions
