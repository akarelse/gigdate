<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
	* this file is part of a gigdate module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a gigdate module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Gigdate Module
 */
 
class GigDate extends Public_Controller
{
	
	/**
	* Constructor method
	*
	* @access public
	* @return void
	*/
	public function __construct()
	{
		parent::__construct();
		$this->load->model('gigdate_m');
		$this->lang->load('gigdate');
		
		$this->template->append_css('module::gigdate.css')
		->append_js('module::gigdate.js');				
	}

	/**
	 * Index method
	 *
	 * @access public
	 * @return void
	 */
	public function index($offset = 0)
	{
		
		$data->events = $this->gigdate_m->get_all('clean');
		if (count($data->events ))
		{
			$data->items_exist = TRUE;
		}
		else
		{
			$data->items_exist = FALSE;
		}
		
		$data->pagination = create_pagination('gigdate', $this->gigdate_m->count_all(), $this->settings->show_nr, 2);

		$this->template->title($this->module_details['name'], 'the rest of the page title')
		->build('index', $data);
	}
	
	/**
	* View a single gigdate
	*
	* @access public
	* @param string $id The id of the gigdate
	* @return void
	*/
	public function event($id = '')
	{
		$item = $this->gigdate_m->get_by('id', $id,'clean');
		$this->template->title($this->module_details['name'], 'the rest of the page title')
		->build('singelevent', $item);
	}
	

}
