<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
	* this file is part of a gigdate module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a gigdate module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Gigdate Module
 */
 
class Admin extends Admin_Controller
{

	protected $section = 'items';
	/**
	 * Validation rules for creating a new gigdate
	 *
	 * @var array
	 * @access private
	 */
	
	private $item_validation_rules = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'published',
				'label' => 'Published',
				'rules' => 'trim|max_length[1]'
			),
			array(
				'field' => 'created_on',
				'label' => 'Created on',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'created_on_minute',
				'label' => 'Created on minute',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'created_on_hour',
				'label' => 'Created on hour',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'dontshowtime',
				'label' => 'Dont show the time',
				'rules' => 'trim|max_length[1]'
			),
			array(
				'field' => 'description',
				'label' => 'Description',
				'rules' => 'trim|max_length[1000]'
			),
			array(
				'field' => 'city',
				'label' => 'City',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'province',
				'label' => 'Province',
				'rules' => 'trim|max_length[100]'
			),
			array(
				'field' => 'avenue',
				'label' => 'Avenue',
				'rules' => 'trim|max_length[100]|required'
			),
			array(
				'field' => 'avenuelink',
				'label' => 'Avenuelink',
				'rules' => 'trim|max_length[150]|prep_url'
			),
			array(
				'field' => 'price',
				'label' => 'Price',
				'rules' => 'trim|max_length[50]|numeric'
			),
			array(
				'field' => 'age',
				'label' => 'Age',
				'rules' => 'trim|max_length[3]|numeric'
			),
			array(
				'field' => 'currency',
				'label' => 'Currency',
				'rules' => 'trim|max_length[1]'
			),
			array(
				'field' => 'support',
				'label' => 'Support',
				'rules' => 'trim|max_length[150]'
			)
		);
		
	private $cur_values = 
		array(
		0 	=> "Default currency",
		1 	=> "Dollars",
		2 	=> "Euros",
		3 	=> "Pound",
		4 	=> "Lang defined"
		);
	
	/**
	* Constructor method
	*
	* @access public
	* @return void
	*/
	 
	public function __construct()
	{
		parent::__construct();

		$this->load->model('gigdate_m');
		$this->load->library('form_validation');
		$this->lang->load('gigdate');
		
		$this->data->cur_values = $this->cur_values;

		$this->template
		->set('hours', array_combine($hours = range(0, 23), $hours))
		->set('minutes', array_combine($minutes = range(0, 59), $minutes));

		$this->template->append_js('module::admin.js')
		->append_js('jquery/jquery.ui.nestedSortable.js')
		->append_css('module::admin.css')
		->append_metadata($this->load->view('fragments/wysiwyg', $this->data, TRUE));
	}

	/**
	 * List all items
	 */
	 
	/**
	* List all existing gigdates
	*
	* @access public
	* @return void
	*/
	public function index()
	{
		$items = $this->gigdate_m->get_all_admin();
		$this->data->items =& $items;
		$this->template->title($this->module_details['name'])
		->build('admin/items', $this->data);
	}
	
	/**
	* Create a new gigdate
	*
	* @access public
	* @return void
	*/
	public function create()
	{

		$this->form_validation->set_rules($this->item_validation_rules);

		
		if ($input = $this->input->post())
		{
	
			$date = $this->input->post('created_on') . ":" . $this->input->post('created_on_hour') . ":" . $this->input->post('created_on_minute') . ":00";
			$data = $this->input->post();
			unset($data['created_on']);
			unset($data['created_on_hour']);
			unset($data['created_on_minute']);
			unset($data['btnAction']);
			$data['date'] = $date;
	
			if ($id = $this->gigdate_m->create($data) && $this->form_validation->run())
			{
				$this->session->set_flashdata('success', lang('gigdate.success'));
				redirect('admin/gigdate');
			}
			else
			{
				$this->session->set_flashdata('error', lang('gigdate.error'));
				redirect('admin/gigdate/create');
			}
		}
		else
		{
			foreach ($this->item_validation_rules AS $rule)
			{
				$this->data->{$rule['field']} = $this->input->post($rule['field']);
			}
			$date = new DateTime();
			$date->setTimestamp(now());
		}
		$this->template->title($this->module_details['name'], lang('gigdate.new_item'))
		->set('rightdate', $date->format('U'))
		->build('admin/form', $this->data);
		
	}
	
	/**
	* Edit an existing gigdate
	*
	* @access public
	* @param int $id The ID of the gigdate to edit
	* @return void
	*/
	
	public function edit($id = 0)
	{
		$this->data = $this->gigdate_m->get($id);
		$this->form_validation->set_rules($this->item_validation_rules);
		if ($input = $this->input->post())
		{
			unset($_POST['btnAction']);
			$date = $this->input->post('created_on') . ":" . $this->input->post('created_on_hour') . ":" . $this->input->post('created_on_minute') . ":00";
			$data = $this->input->post();
			unset($data['created_on']);
			unset($data['created_on_hour']);
			unset($data['created_on_minute']);
			unset($data['btnAction']);
			$data['date'] = $date;
			
			if ($this->gigdate_m->edit($id,$data) && $this->form_validation->run())
			{
				$this->session->set_flashdata('success', lang('gigdate.success'));
				redirect('admin/gigdate');
			}
			else
			{
				$this->session->set_flashdata('error', lang('gigdate.error'));
				redirect('admin/gigdate/create');
			}
		}
		

		$date = new DateTime();
		$date->setTimestamp(now());
		if ($this->data->date)
		{
			$date = $date->createFromFormat('Y-m-d H:i:s', $this->data->date);
		}
		
		$this->template->title($this->module_details['name'], lang('gigdate.edit'))
		->set('rightdate', $date->format('U'))
		->build('admin/form', $this->data);
	}
	
	/**
	* Delete an existing gigdate
	*
	* @access public
	* @param int $id The ID of the gigdate to delete
	* @return void
	*/
	
	public function delete($id = 0)
	{
		// make sure the button was clicked and that there is an array of ids
		if (isset($_POST['btnAction']) AND is_array($_POST['action_to']))
		{
			$this->gigdate_m->delete_many($this->input->post('action_to'));
		}
		elseif (is_numeric($id))
		{
			$this->gigdate_m->delete($id);
		}
		redirect('admin/gigdate');
	}
}
