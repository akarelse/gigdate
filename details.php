<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
	* this file is part of a gigdate module for pyrocms
	* Copyright (C) 2012  Aat Karelse <aat@vuurrosmedia.nl>
	* This program is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.

    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.

    * You should have received a copy of the GNU General Public License
    * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * This is a gigdate module for PyroCMS
 *
 * @author 		Aat Karelse
 * @website		http://vuurrosmedia.nl
 * @package 	PyroCMS
 * @subpackage 	Gigdate Module
 */

class Module_GigDate extends Module {

	public $version = '1.0.0';
	const MIN_PHP_VERSION = '5.3.0';

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'Gig date'
			),
			'description' => array(
				'en' => 'This is a gig date list'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => 'content',
			'sections' => array(
				'items' => array(
					'name' 	=> 'gigdate:items',
					'uri' 	=> 'admin/gigdate',
						'shortcuts' => array(
							'create' => array(
								'name' 	=> 'gigdate:create',
								'uri' 	=> 'admin/gigdate/create',
								'class' => 'add'
								)
							)
						)
				)
		);
	}

	public function install()
	{
		if (!$this->check_php_version())
		{
			return FALSE;
		}
		

		$gigdate = array(

                    'id' => array(
									  'type' => 'INT',
									  'constraint' => 11,
									  'auto_increment' => TRUE,
									  'primary' => true
									  ),
						'date' => array(
										'type' => 'DATETIME',
										'null' => false
										),
						'published' => array(
										'type' => 'TINYINT',
										'constraint' => 1,
										'default' => '0'
										),
						'dontshowtime' => array(
										'type' => 'TINYINT',
										'constraint' => 1,
										'default' => '0'
										),
						'name' => array(
										'type' => 'VARCHAR',
										'constraint' => 100
										),
						'description' => array(
										'type' => 'VARCHAR',
										'constraint' => 100,
										'null' => false
										),
						'city' => array(
										'type' => 'VARCHAR',
										'constraint' => 100,
										'null' => false
										),
						'province' => array(
										'type' => 'VARCHAR',
										'constraint' => 100,
										'null' => false
										),
						'avenue' => array(
										'type' => 'VARCHAR',
										'constraint' => 100,
										'null' => false
										),
						'avenuelink' => array(
										'type' => 'VARCHAR',
										'constraint' => 100,
										'null' => false
										),
						'price' => array(
										'type' => 'VARCHAR',
										'constraint' => 10,
										'null' => false
										),
						'age' => array(
										'type' => 'VARCHAR',
										'constraint' => 3,
										'null' => false
										),
						'currency' => array(
										'type' => 'TINYINT',
										'constraint' => 1,
										'null' =>	false
										),		
						'support' => array(
										'type' => 'VARCHAR',
										'constraint' => 100
										)
						);

		$gigdate_setting_extrabar = array(
				'slug' => 'gig_extra',
				'title' => 'Gig extra',
				'description' => 'Enable the extra info section of gig date results. (only for direct controller, not widgets)',
				'`default`' => '1',
				'`value`' => '1',
				'type' => 'select',
				'`options`' => '1=Yes|0=No',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'gigdate'
		);
		
		$gigdate_setting_ascdesc = array(
				'slug' => 'gig_ascdesc',
				'title' => 'Gig Ascending descending on date',
				'description' => 'Sort the gig dates asending or descending.(only for direct controller, not widgets)',
				'`default`' => 'ASC',
				'`value`' => 'ASC',
				'type' => 'select',
				'`options`' => 'ASC=Ascending|DESC=Descending',
				'is_required' => 1,
				'is_gui' => 1,
				'module' => 'gigdate'
		);
		
		$gigdate_setting_show_after = array(
			'slug' => 'show_after',
			'title' => 'Show after',
			'description' => 'Nr of day`s before gigdate is removed from gigdatewidget and go`s tot pastgigdate widget after the gig.',
			'`default`' => '0',
			'`value`' => '50',
			'type' => 'text',
			'`options`' => '',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'gigdate'
		);

		
		$gigdate_setting_show_nr = array(
			'slug' => 'show_nr',
			'title' => 'Show Nr',
			'description' => 'Number of events to show.(only for direct controller, not widgets)',
			'`default`' => '0',
			'`value`' => '10',
			'type' => 'text',
			'`options`' => '',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'gigdate'
		);
		
		$this->db->trans_start();

		$this->db->insert('settings', $gigdate_setting_extrabar);
		$this->db->insert('settings', $gigdate_setting_ascdesc);
		$this->db->insert('settings', $gigdate_setting_show_after);
		$this->db->insert('settings', $gigdate_setting_show_nr);
		
		$this->dbforge->add_field($gigdate);
		$this->dbforge->add_key('id', TRUE);

		$this->dbforge->create_table('gig_date');
	
		
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	public function uninstall()
	{
		
		if ($this->db->delete('settings', array('module' => 'gigdate')) && $this->dbforge->drop_table('gig_date'))
		{
			return TRUE;
		}
		return FALSE;
	}


	public function upgrade($old_version)
	{
		if ($this->install() && $this->uninstall())
		{
		return TRUE;
		}
		return false;
	}

	public function help()
	{
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
	
	/**
	 * Check the current version of PHP and thow an error if it's not good enough
	 *
	 * @access private
	 * @return boolean
	 * @author Victor Michnowicz
	 */
	private function check_php_version()
	{
		// If current version of PHP is not up snuff
		if ( version_compare(PHP_VERSION, self::MIN_PHP_VERSION) < 0 )
		{
			show_error('This addon requires PHP version ' . self::MIN_PHP_VERSION . ' or higher.');
			return FALSE;
		}
      return TRUE;
	}
}
/* End of file details.php */
